package com.example.firstservice.it.controller;

import com.example.firstservice.domain.Subscription;
import com.example.firstservice.domain.User;
import com.example.firstservice.it.BaseIntegTest;
import com.example.firstservice.repos.SubscriptionRepo;
import com.example.firstservice.repos.UserRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Testcontainers(disabledWithoutDocker = true)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SpringExtension.class)
@Transactional
public class SubscriptionControllerIntegTest extends BaseIntegTest {
    @Autowired
    private WebApplicationContext webContext;
    private MockMvc mockMvc;
    private User followedUser;
    private User followerUser;
    @Autowired
    private UserRepo userRepo;
    private static KafkaConsumer<String, String> kafkaConsumer;
    @Autowired
    private SubscriptionRepo subscriptionRepo;

    @BeforeEach
    public void init() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();

        followerUser = new User();
        followerUser.setUserName("MikhailJ");
        followerUser.setFirstName("Mikhail");
        followerUser.setSecondName("Sergeevich");
        followerUser.setLastName("Petrov");
        followerUser.setEmailAddress("dummy@example.com");
        followerUser.setPhoneNumber("2223344555");
        followerUser.setPassword("password1");
        followerUser.setId(1L);

        followedUser = new User();
        followedUser.setUserName("MikhailY");
        followedUser.setFirstName("Mikhail");
        followedUser.setSecondName("Ivanovich");
        followedUser.setPassword("password");
        followedUser.setLastName("Ivanov");
        followedUser.setEmailAddress("dummy_yammy@example.com");
        followedUser.setPhoneNumber("2223344555");
        followedUser.setId(2L);

        kafkaConsumer = getKafkaConsumer();
        kafkaConsumer.subscribe(List.of("statistics"));
        followerUser = userRepo.save(followerUser);
        Assertions.assertNotNull(followerUser.getId());
        followedUser = userRepo.save(followedUser);
        Assertions.assertNotNull(followedUser.getId());
    }

    @AfterAll
    static void close() {
        // Close the consumer before shutting down Testcontainers Kafka instance
        kafkaConsumer.close();
    }

    @Test
    void subscribeTest() throws Exception{
        Subscription subs = new Subscription();
        subs.setFollowed(followedUser);
        subs.setFollower(followerUser);
        ObjectMapper objectMapper = new ObjectMapper();
        String subsJson = objectMapper.writeValueAsString(subs);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/subscription/subscribe/v2")
                .header(AUTHORIZATION, "Bearer " + getToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(subsJson));
        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$",
                        Matchers.equalTo("Subscription for follower user:" +
                                " MikhailJ and followed user: MikhailY has been saved in the database")));
        assertDoesNotThrow(
                () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, "statistics", Duration.ofSeconds(5)), "No records found for topic");

    }


    @Test
    void getSubscriptionByIDTest() throws Exception{
        Subscription subs = new Subscription();
        subs.setFollowed(followedUser);
        subs.setFollower(followerUser);
        subs = subscriptionRepo.save(subs);
        Assertions.assertNotNull(subs.getId());
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/subscription/getSubscription/{id}", subs.getId())
                .header(AUTHORIZATION, "Bearer " + getToken())
                .contentType(MediaType.APPLICATION_JSON));
        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(subs.getId()));

    }

    @Test
    void deleteSubscriptionByIDTest() throws Exception{
        Subscription subs = new Subscription();
        subs.setFollowed(followedUser);
        subs.setFollower(followerUser);
        subs = subscriptionRepo.save(subs);
        Assertions.assertNotNull(subs.getId());
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/subscription/deleteSubscription/{id}", 1)
                .header(AUTHORIZATION, "Bearer " + getToken())
                .contentType(MediaType.APPLICATION_JSON));
        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$",
                        Matchers.equalTo("Subscription has been deleted from the database")));

    }
}
