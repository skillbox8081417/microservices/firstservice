package com.example.firstservice.it.client;

import com.example.firstservice.client.ImageClient;
import com.example.firstservice.it.BaseIntegTest;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Testcontainers(disabledWithoutDocker = true)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SpringExtension.class)
public class ImageClientIntegTest extends BaseIntegTest {
    @Autowired
    private ImageClient imageClient;
    private WireMockServer mockImageService;
    private static final String FILE_NAME = "test-image.jpg";
    @MockBean
    private JwtDecoder jwtDecoder;

    @BeforeEach
    public void init() throws IOException {
        mockImageService = new WireMockServer(
                new WireMockConfiguration().port(9611)
        );
        mockImageService.start();
        WireMock.configureFor("localhost", 9611);

    }
    @AfterEach
    public void stop () {
        mockImageService.stop();
    }

    @Test
    void testUploadProfileImage_thenVerify() throws Exception {
        mockImageService.stubFor(WireMock.post(WireMock.urlMatching(".*/images/upload.*?"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withBody("Photo 'test-image.jpg' was successfully uploaded")));
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(FILE_NAME).getFile());
        Assert.assertTrue(file.exists());
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("image", FILE_NAME,
                MediaType.MULTIPART_FORM_DATA_VALUE, IOUtils.toByteArray(input));
        Assert.assertEquals("Photo 'test-image.jpg' was successfully uploaded",imageClient.uploadImage(multipartFile));
    }

    @Test
    void testDeleteProfileImage_thenVerify() throws IOException {
        mockImageService.stubFor(WireMock.delete(WireMock.urlMatching(".*/images/deleteImage.*?"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withBody("Photo 'test-image.jpg' was successfully deleted")));
        Assert.assertEquals("Photo 'test-image.jpg' was successfully deleted",imageClient.deleteImage(FILE_NAME));
    }
}
