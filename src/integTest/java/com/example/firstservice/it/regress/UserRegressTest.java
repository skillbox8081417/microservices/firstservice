package com.example.firstservice.it.regress;

import com.example.firstservice.domain.Subscription;
import com.example.firstservice.domain.User;
import com.example.firstservice.repos.SubscriptionRepo;
import com.example.firstservice.repos.UserRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Optional;

@DataJpaTest
@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@RunWith(SpringRunner.class)
public class UserRegressTest {
    @Autowired
    UserRepo userRepo;
    @Autowired
    private SubscriptionRepo subsRepo;

    @Test
    @Transactional
    public void testCreateUser_andCount() {
        User followerUser = new User();
        followerUser.setUserName("MikhailJ");
        followerUser.setFirstName("Mikhail");
        followerUser.setSecondName("Sergeevich");
        followerUser.setLastName("Petrov");
        followerUser.setEmailAddress("dummy@example.com");
        followerUser.setPhoneNumber("2223344555");
        followerUser.setPassword("password1");
        followerUser.setId(1L);

        followerUser = userRepo.save(followerUser);
        Assertions.assertEquals(1, userRepo.count());

        User followedUser = new User();
        followedUser.setUserName("MikhailY");
        followedUser.setFirstName("Mikhail");
        followedUser.setSecondName("Ivanovich");
        followedUser.setPassword("password");
        followedUser.setLastName("Ivanov");
        followedUser.setEmailAddress("dummy_yammy@example.com");
        followedUser.setPhoneNumber("2223344555");
        followedUser.setId(2L);

        followedUser = userRepo.save(followedUser);
        Assertions.assertEquals(2, userRepo.count());

        Optional<User> followerUserById = userRepo.findById(followedUser.getId());
        Assertions.assertTrue(followerUserById.isPresent());
        Optional<User> followedUserById = userRepo.findById(followedUser.getId());
        Assertions.assertTrue(followedUserById.isPresent());

        Subscription subs = new Subscription();
        subs.setFollowed(followedUser);
        subs.setFollower(followerUser);

        subs = subsRepo.save(subs);
        Optional<Subscription> subById = subsRepo.findById(subs.getId());
        Assertions.assertTrue(subById.isPresent());
        subsRepo.deleteById(subs.getId());

        subById = subsRepo.findById(subs.getId());
        Assertions.assertTrue(subById.isEmpty());

        followedUser.setAbout("Some text about me");
        User updatedFollowedUser = userRepo.save(followedUser);
        Assertions.assertEquals("Some text about me", updatedFollowedUser.getAbout());

        userRepo.deleteById(followedUser.getId());
        followerUserById = userRepo.findById(followedUser.getId());
        Assertions.assertTrue(followerUserById.isEmpty());
        userRepo.deleteById(followedUser.getId());
        followedUserById = userRepo.findById(followedUser.getId());
        Assertions.assertTrue(followedUserById.isEmpty());

    }
}
