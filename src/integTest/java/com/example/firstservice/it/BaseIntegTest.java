package com.example.firstservice.it;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dasniko.testcontainers.keycloak.KeycloakContainer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@DirtiesContext
public class BaseIntegTest {

    @Container
    private final static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.5.1"));
    //KEYCLOAK (1)
    @Container
    private static final KeycloakContainer keycloakContainer = new KeycloakContainer()
            .withRealmImportFile("realm-config.json")
            .withEnv("DB_VENDOR", "h2");

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        kafka.start();
        //KEYCLOAK (2)
        registry.add("spring.security.oauth2.resourceserver.jwt.jwk-set-uri",
                () -> keycloakContainer.getAuthServerUrl() + "/realms/skillbox-microservices/protocol/openid-connect/certs");
        registry.add("spring.kafka.bootstrap-servers", kafka::getBootstrapServers);
    }

    protected static KafkaConsumer<String,String> getKafkaConsumer() {
        var consumerProps = KafkaTestUtils.consumerProps(kafka.getBootstrapServers(), "test-consumer", "true");
        consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(consumerProps);
        return kafkaConsumer;
    }

    // KEYCLOAK (3)
    private final String authServerUrl = keycloakContainer.getAuthServerUrl() + "/realms/skillbox-microservices/protocol/openid-connect/token";

    // KEYCLOAK (4)
    protected String getToken() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("grant_type", Collections.singletonList("client_credentials"));
        map.put("client_id", Collections.singletonList("microservice-auth"));
        map.put("client_secret", Collections.singletonList("3g9N7xHZVXZXRYf1Asvh3GPUVXQXi9t2"));
        KeyCloakToken token =
                restTemplate.postForObject(
                        authServerUrl, new HttpEntity<>(map, httpHeaders), KeyCloakToken.class);

        assertThat(token).isNotNull();
        return token.getAccessToken();
    }

    private static class KeyCloakToken {

        private final String accessToken;

        @JsonCreator
        KeyCloakToken(@JsonProperty("access_token") final String accessToken) {
            this.accessToken = accessToken;
        }

        public String getAccessToken() {
            return accessToken;
        }
    }
}
