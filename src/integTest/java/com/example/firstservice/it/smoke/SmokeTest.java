package com.example.firstservice.it.smoke;

import com.example.firstservice.it.BaseIntegTest;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Testcontainers(disabledWithoutDocker = true)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SpringExtension.class)
public class SmokeTest extends BaseIntegTest {
    @Autowired
    private WebApplicationContext webContext;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
        jdbcTemplate.execute("ALTER SEQUENCE users_seq RESTART WITH 1");
    }

    @Test
    @Transactional
    void createUser() throws Exception {
        JSONObject request = new JSONObject();
        request.put("userName","VasyaM");
        request.put("emailAddress", "dummy@elogex.com");
        request.put("firstName", "Vasya");
        request.put("secondName", "Petrovich");
        request.put("lastName", "Myslov");
        request.put("password", "test");
        request.put("gender", "man");
        request.put("phoneNumber", "8800222334");

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/user/createUser")
                .header(AUTHORIZATION, "Bearer " + getToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(request.toString()));
        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("User with userName: VasyaM has been saved in the database with id 1")));
    }
}
