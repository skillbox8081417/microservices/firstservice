package com.example.firstservice.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

public record EventRecord(
        @NotNull
        UUID uuid,
        LocalDateTime dateTime,
        String objectType,
        String objectId,
        String eventInitiator) {
    public @Builder EventRecord{};
}
