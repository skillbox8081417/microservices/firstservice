package com.example.firstservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;

@FeignClient (name = "image-api", url = "${app.services.image-api.url}")
public interface ImageClient {

    @GetMapping(value = "/images/getImage")
    File getImage(@RequestParam("fileName") String fileName) throws ResponseStatusException;

    @PostMapping (value = "/images/upload")
    String uploadImage(@RequestParam("image") MultipartFile image) throws IOException;

    @DeleteMapping (value = "/images/deleteImage")
    String deleteImage(@RequestParam("fileName") String filename) throws IOException;
}
