package com.example.firstservice.service;

import com.example.firstservice.domain.Subscription;
import com.example.firstservice.domain.User;
import com.example.firstservice.dto.EventRecord;
import com.example.firstservice.producer.EventKafkaProducer;
import com.example.firstservice.repos.SubscriptionRepo;
import com.example.firstservice.repos.UserRepo;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class SubscriptionService {
    private final SubscriptionRepo subsRepo;
    private final EventKafkaProducer subsProducer;
    private final UserRepo userRepo;
    private static final String USER_NOT_FOUND = "User with userName: %s not found in the database";
    public SubscriptionService (SubscriptionRepo subsRepo, EventKafkaProducer subsProducer, UserRepo userRepo) {
        this.subsRepo = subsRepo;
        this.subsProducer = subsProducer;
        this.userRepo = userRepo;
    }

    public String createSubscription(String followerUserName, String followedUserName) {
        User followerUser = userRepo.findByUserName(followerUserName);
        User followedUser = userRepo.findByUserName(followedUserName);
        if (followerUser == null) {
            return String.format(USER_NOT_FOUND, followerUserName);
        }
        if (followedUser == null) {
            return String.format(USER_NOT_FOUND, followedUserName);
        }

        Subscription subscription = new Subscription();
        subscription.setFollower(followerUser);
        subscription.setFollowed(followedUser);
        subscription.setActive(true);
        Subscription savedSubscription = subsRepo.save(subscription);
        return String.format("Subscription for follower user: %s and followed user: %s has been saved in the database",
                savedSubscription.getFollower().getUserName(), savedSubscription.getFollowed().getUserName());
    }

    public String createSubscription(Subscription subscription) throws ExecutionException, InterruptedException {
        User followerUser = userRepo.findByUserName(subscription.getFollower().getUserName());
        User followedUser = userRepo.findByUserName(subscription.getFollowed().getUserName());
        if (followerUser == null) {

            return String.format(USER_NOT_FOUND, subscription.getFollower().getUserName());
        }
        if (followedUser == null) {
            return String.format(USER_NOT_FOUND, subscription.getFollowed().getUserName());
        }
        subscription.setActive(true);
        Subscription savedSubscription = subsRepo.save(subscription);
        var event = buildEvent(savedSubscription);
        subsProducer.sendMessage(event);
        return String.format("Subscription for follower user: %s and followed user: %s has been saved in the database",
                savedSubscription.getFollower().getUserName(), savedSubscription.getFollowed().getUserName());
    }

    public String deleteSubscription(Long id) {
        if (!subsRepo.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        subsRepo.deleteById(id);
        return String.format("Subscription has been deleted from the database");
    }

    public List<Subscription> getSubscriptions() {
        return subsRepo.findAll();
    }

    public List<Subscription> getSubscriptionsByFollower(String followerUserName) {
        List<Subscription> subscriptions = subsRepo.findByFollower_UserName(followerUserName);
        if (CollectionUtils.isEmpty(subscriptions)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return subscriptions;
    }

    public Subscription getSubscription(Long id) {
        if (!subsRepo.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return subsRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    private EventRecord buildEvent(Subscription subs) {
        EventRecord event = EventRecord.builder()
                .uuid(UUID.randomUUID())
                .eventInitiator(subs.getFollower().getId().toString())
                .objectId(subs.getFollowed().getId().toString())
                .objectType("Subscription")
                .dateTime(LocalDateTime.now())
                .build();
        return event;
    }
}
