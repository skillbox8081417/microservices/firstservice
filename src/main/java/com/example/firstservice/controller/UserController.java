package com.example.firstservice.controller;

import com.example.firstservice.client.ImageClient;
import com.example.firstservice.domain.User;
import com.example.firstservice.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ImageClient imageClient;

    @PostMapping("createUser")
    @Operation(summary = "Create new user")
    public String createUser(@RequestBody User user){
        log.info("UserController: create user: ", user.getUserName());
        return userService.createUser(user);
    }

    @GetMapping("/getUserList")
    @Operation(summary = "Get users list")
    public List<User> getUsers() {
        log.info("UserController get users list");
        return userService.getUsers();
    }

    @GetMapping("/getUser/{id}")
    @Operation(summary = "Get users by user surrogate key")
    public  User getUser(@PathVariable Long id) {
        log.info("UserController get user by id: " + id);
        return userService.getUser(id);
    }

    @PutMapping("/putUser/{id}")
    @Operation(summary = "Update user information")
    public String updateUser (@RequestBody User user, @PathVariable Long id) {
        log.info("UserController update user info");
        return userService.updateUser(user,id);
    }

    @DeleteMapping("/deleteUser/{id}")
    @Operation(summary = "Delete user")
    public String deleteUser(@PathVariable Long id) {
        log.info("UserController delete user by surrogate id: " + id);
        return userService.deleteUser(id);
    }

    @PostMapping(value = "/updateUserProfileImage/{id}")
    @Operation(summary = "Update user profile image")
    public String updateProfileImage(@PathVariable Long id, @RequestParam("image") MultipartFile image) {
        log.info("UserController prepare to save user profile image");
        String uuidFile = UUID.randomUUID().toString();
        String resultFileName = uuidFile + "." + image.getOriginalFilename();
        User user = userService.getUser(id);
        try {
            String result = imageClient.uploadImage(image);
            if (result != null) {
                log.info("UserController saving user profile image");
                user.setImageRef(resultFileName);
                log.info("UserController updating user with image ref");
                result += " " + userService.updateUser(user, user.getId());
            }
            return result;
        } catch (IOException e) {
            throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "File with name: " + image.getOriginalFilename() + "cannot be uploaded");
        }
    }

    @PostMapping("/deleteUserProfileImage/{id}")
    @Operation(summary = "Delete User profile image")
    public String deleteProfileImage(@PathVariable Long userId) {
        User user = userService.getUser(userId);
        String imageRef = user.getImageRef();
        log.info("UserController preparing to delete profile image: " + imageRef);
        try {
            String result = imageClient.deleteImage(imageRef);
            if (result != null) {
                log.info("UserController deleting profile image: " + imageRef);
                user.setImageRef(null);
                result += " " + userService.updateUser(user, user.getId());
                log.info("UserController nullifying user image ref");
            }
            return result;
        } catch (IOException e) {
            throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "File with name: " + imageRef + "cannot be deleted");
        }
    }

    @GetMapping("/deleteUserProfileImage/{id}")
    @Operation(summary = "Retrieve User profile image")
    public File getProfileImage(@PathVariable Long userId) {
        User user = userService.getUser(userId);
        String imageRef = user.getImageRef();
        log.info("UserController retrieving user profile image");
        File result = imageClient.getImage(imageRef);
        return result;
    }
}
