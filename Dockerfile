FROM openjdk:19
ARG JAR_FILE=build/libs/\*.jar
COPY ${JAR_FILE} app.jar

EXPOSE 8008
ENTRYPOINT exec java $JAVA_OPTS -jar app.jar


