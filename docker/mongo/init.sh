#!/bin/bash

mongosh --tls \
  --tlsCAFile /data/ssl/rootCA.cert.pem \
  --tlsCertificateKeyFile /data/ssl/server.pem <<EOF

use admin
db = db.getSiblingDB('$MONGODB_DB');
db.createUser({
  user: '$MONGODB_USER',
  pwd:  '$MONGODB_PASSWORD',
  roles: [{
    role: 'readWrite',
    db: '$MONGODB_DB'
  }]
})
EOF