#!/usr/bin/env bash
set -ex

###########################
#- VARS
###########################
#--- Domain name
DNL=localhost
DNM=mongo
DNS=statistics
DNC=comments

###########################
#--- INTERNAL ROOT CA VARS
###########################
ROOT=rootCA
ROOT_PATH=./$ROOT
ROOT_PASSWORD=$ROOT-password
###########################
#--- RESOURCE SERVER VARS
###########################
MONGO=mongo
MONGO_PATH=./$MONGO
###########################
#--- DOCKER IMPORT VARS
###########################
PATH_TO_COPY=../imports/$MONGO/certs
PATH_TO_INIT=../imports/$MONGO/init

mkdir -p $MONGO_PATH
mkdir -p $PATH_TO_COPY

#--------------------------------------Postgresql (as SSL-SERVER) cert & key---------------------------------------#
#
# 1 Генерим приватный ключ PEM
#             ->
#               2 Создаем запрос в CA на подпись сертификата .csr
#                     ->
#                       3 Создаем файл конфигурации для настройки разрешений сертификата и подписываем сертификат Postgre



# 1.####################################
# Генерим приватный ключ PEM для Postgres
# Input: -
# Command: genkeypair
# Output: файл приватного ключа .key
########################################
openssl genpkey -out $MONGO_PATH/$MONGO.pkcs8.key -outform PEM -algorithm RSA
# for old postgres version the pkcs1 format should be used
#openssl rsa -in $MONGO_PATH/$MONGO.pkcs8.key -outform PEM -out $MONGO_PATH/$MONGO.pkcs1.key -traditional

# 2.####################################
# Создаем запрос в CA на подпись сертификата
# Input: приватный ключ с шага 1
# Command: certreq
# Output: .csr request file
########################################
openssl req -verbose -new -nodes -sha256 \
        -key $MONGO_PATH/$MONGO.pkcs8.key \
        -subj "//CN=$MONGO" \
        -out $MONGO_PATH/$MONGO.csr

# 3.####################################
# Имитация работы CA.
# Создаем файл конфигурации для настройки разрешений сертификата
# и подписываем сертификат Postgre
# Input: Root CA key and his key password, .csr request, extension params
# Command: gencert
# Output: .crt файл формата PEM
########################################

# Шаг 1. Файл конфигурации .ext для использования в процессе подписания
# через subjectAltName задается DNS, доступ с которого будет разрешен
>$MONGO_PATH/$MONGO.ext cat <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth,clientAuth
keyUsage = digitalSignature, nonRepudiation, dataEncipherment, keyEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = $DNL # Be sure to include the domain name here because Common Name is not so commonly honoured by itself
DNS.2 = $DNC
DNS.2 = $DNM
DNS.2 = $DNS
IP.1 = 127.0.0.1 # Optionally, add an IP address (if the connection which you have planned requires it)
EOF

# Шаг 2. Имитируем работу с запросом .csr, т.е. подписываем сертификат в Root CA
openssl x509 -req -sha256 \
    -in $MONGO_PATH/$MONGO.csr \
    -CA $ROOT_PATH/$ROOT.cert.pem \
    -passin pass:$ROOT_PASSWORD \
    -CAkey $ROOT_PATH/$ROOT.key \
    -CAcreateserial \
    -CAserial $MONGO_PATH/$MONGO.srl \
    -days 365 \
    -extfile $MONGO_PATH/$MONGO.ext \
    -out $MONGO_PATH/$MONGO.crt

########################################
# Copies result to docker import
########################################
cp $MONGO_PATH/$MONGO.pkcs8.key $PATH_TO_COPY/server.key
cp $MONGO_PATH/$MONGO.crt $PATH_TO_COPY/server.crt

cat "$PATH_TO_COPY/server.key" "$PATH_TO_COPY/server.crt" > "$PATH_TO_COPY/server.pem"
