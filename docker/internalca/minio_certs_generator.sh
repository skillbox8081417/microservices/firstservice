#!/usr/bin/env bash
set -ex

###########################
#- VARS
###########################
#--- Domain name
DNL=localhost
DND=discovery-service
DNU=user-service
DNG=gateway-service
DNF=file-service
DNK=keycloak
DNM=minio
###########################
#--- INTERNAL ROOT CA VARS
###########################
ROOT=rootCA
ROOT_PATH=./$ROOT
ROOT_PASSWORD=$ROOT-password
###########################
#--- RESOURCE SERVER VARS
###########################
MINIO=minio
MINIO_PATH=./$MINIO
###########################
#--- DOCKER IMPORT VARS
###########################
PATH_TO_COPY=../imports/$MINIO/certs

mkdir -p $MINIO_PATH
mkdir -p $PATH_TO_COPY

#--------------------------------------Minio (as SSL-SERVER) cert & key---------------------------------------#
#
# 1 Генерим приватный ключ PEM
#             ->
#               2 Создаем запрос в CA на подпись сертификата .csr
#                     ->
#                       3 Создаем файл конфигурации для настройки разрешений сертификата и подписываем сертификат Minio



# 1.####################################
# Генерим приватный ключ PEM для Minio
# Input: -
# Command: genkeypair
# Output: файл приватного ключа .key
########################################
openssl genpkey -out $MINIO_PATH/$MINIO.pkcs8.key -outform PEM -algorithm RSA

# 2.####################################
# Создаем запрос в CA на подпись сертификата
# Input: приватный ключ с шага 1
# Command: certreq
# Output: .csr request file
########################################
openssl req -verbose -new -nodes -sha256 \
        -key $MINIO_PATH/$MINIO.pkcs8.key \
        -subj "//CN=$MINIO" \
        -out $MINIO_PATH/$MINIO.csr

# 3.####################################
# Имитация работы CA.
# Создаем файл конфигурации для настройки разрешений сертификата
# и подписываем сертификат minio
# Input: Root CA key and his key password, .csr request, extension params
# Command: gencert
# Output: .crt файл формата PEM
########################################

# Шаг 1. Файл конфигурации .ext для использования в процессе подписания
# через subjectAltName задается DNS, доступ с которого будет разрешен
>$MINIO_PATH/$MINIO.ext cat <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth,clientAuth
keyUsage = digitalSignature, nonRepudiation, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = $DNL # Be sure to include the domain name here because Common Name is not so commonly honoured by itself
DNS.2 = $DNF
DNS.3 = $DNK
DNS.4 = $DNM
# DNS.2 = $MINIO.$DN # Optionally, add additional domains (I've added a subdomain here)
IP.1 = 127.0.0.1 # Optionally, add an IP address (if the connection which you have planned requires it)
EOF

# Шаг 2. Имитируем работу с запросом .csr, т.е. подписываем сертификат в Root CA
openssl x509 -req -sha256 \
    -in $MINIO_PATH/$MINIO.csr \
    -CA $ROOT_PATH/$ROOT.cert.pem \
    -passin pass:$ROOT_PASSWORD \
    -CAkey $ROOT_PATH/$ROOT.key \
    -CAcreateserial \
    -CAserial $MINIO_PATH/$MINIO.srl \
    -days 365 \
    -extfile $MINIO_PATH/$MINIO.ext \
    -out $MINIO_PATH/$MINIO.crt

########################################
# Copies result to docker import
########################################
cp $MINIO_PATH/$MINIO.pkcs8.key $PATH_TO_COPY/private.key
cp $MINIO_PATH/$MINIO.crt $PATH_TO_COPY/public.crt
